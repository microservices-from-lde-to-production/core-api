FROM node:12

ADD ./ /src
WORKDIR /src

EXPOSE 3000

RUN npm install
RUN npm run build

CMD npm run start:prod
