import { Module } from '@nestjs/common';
import {ApiModule} from "./api/api.module";
import {DalModule} from "./dal/dal.module";
import {InfraModule} from "./infra/infra.module";

@Module({
  imports: [ApiModule, DalModule, InfraModule]
})
export class AppModule {}
