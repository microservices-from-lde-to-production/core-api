import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ConfigService} from 'nestjs-config';
import {Post} from "@dal/posts/post.entity";

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            inject: [ConfigService],
            useFactory: (config: ConfigService) => config.get('connection'),
        }),
        TypeOrmModule.forFeature([Post]),
    ],
    exports: [TypeOrmModule],
})

export class DalModule {
}
