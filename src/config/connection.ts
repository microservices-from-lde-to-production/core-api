import * as ormconfig from '../ormconfig';

export default {
  ...ormconfig,
  host: process.env.POSTGRES_DB_HOST || 'postgres',
  database: process.env.POSTGRES_DB_NAME || 'postgres',
  port: process.env.POSTGRES_DB_PORT || '5432',
  username: process.env.POSTGRES_DB_USER,
  password: process.env.POSTGRES_DB_PASSWORD,
};
