import { Controller } from "@nestjs/common";
import { Crud, CrudController } from "@nestjsx/crud";

import {Post} from "@dal/posts/post.entity";
import {PostsService} from "@app/posts/posts.service";

@Crud({
    model: {
        type: Post,
    },
})
@Controller("posts")
export class ApiPostsController implements CrudController<Post> {
    constructor(public service: PostsService) {}
}
