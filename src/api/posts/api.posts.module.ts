import { Module } from '@nestjs/common';
import {ApiPostsController} from "./api.posts.controller";
import {PostsModule} from "@app/posts/posts.module";

@Module({
    imports: [PostsModule],
    controllers: [ApiPostsController],
})
export class ApiPostsModule {}
