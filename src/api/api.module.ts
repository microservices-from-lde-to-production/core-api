import { Module } from '@nestjs/common';
import {ApiPostsModule} from "./posts/api.posts.module";


@Module({
  imports: [ApiPostsModule],
  controllers: [],
})
export class ApiModule {}
