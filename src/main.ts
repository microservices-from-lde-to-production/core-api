import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import swagger from '@infra/swagger';
import { ConfigService } from 'nestjs-config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  swagger(app, ConfigService.get('app.swaggerPath'));
  await app.listen(ConfigService.get('app.port'));
  return app;
}
export default bootstrap();
