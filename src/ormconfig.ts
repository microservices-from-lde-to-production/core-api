import { ConnectionOptions } from 'typeorm';

const config: ConnectionOptions = {
  type: 'postgres',

  // how to generate migration with cli: https://typeorm.io/#/using-cli/if-entities-files-are-in-typescript
  // how to write migrations with help of built in QueryBuilder: https://typeorm.io/#/select-query-builder
  migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
  entities: [__dirname + '/**/*.entity{.ts,.js}'],

  migrationsRun: true,
  logging: ['schema', 'error'],

  cli: {
    // Location of migration should be inside src folder to be compiled into dist/ folder.
    migrationsDir: 'src/migrations',
  },

  // not recommended in prod mode, migrations are more preferable;
  synchronize: false,

  // for local usage of:  "npm run typeorm:revert" command, "npm run typeorm:generate"
  host: process.env.POSTGRES_DB_HOST || 'postgres',
  database: process.env.POSTGRES_DB_NAME || 'postgres',
  port: parseInt(process.env.POSTGRES_DB_PORT, 10) || 5432,
  username: process.env.POSTGRES_DB_USER,
  password: process.env.POSTGRES_DB_PASSWORD,
};

export = config;
