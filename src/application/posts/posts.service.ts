import { Injectable } from "@nestjs/common";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import {Post} from "@dal/posts/post.entity";
import {PostRepository} from "@dal/posts/post.repository";

@Injectable()
export class PostsService extends TypeOrmCrudService<Post> {
    constructor(
        private readonly postRepository: PostRepository
    ) {
        super(postRepository);
    }
}
