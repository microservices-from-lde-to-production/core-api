import { Module } from '@nestjs/common';
import {PostsService} from "@app/posts/posts.service";
import {DalModule} from "@dal/dal.module";


@Module({
    imports: [DalModule],
    providers: [PostsService],
    exports: [PostsService],
})
export class PostsModule{
}
