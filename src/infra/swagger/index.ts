import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';

export default (app, path = 'documentation') => {
    const options = new DocumentBuilder()
        .setTitle(process.env.npm_package_name)
        .setDescription(process.env.npm_package_description)
        .setVersion(process.env.npm_package_version)
        // .setSchemes('https', 'http')
        // .addBearerAuth()
        .build();

    const document = SwaggerModule.createDocument(app, options);

    SwaggerModule.setup(path, app, document);

    return app;
};
