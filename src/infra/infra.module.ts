import * as path from 'path';
import * as fs from 'fs';

import { Module } from '@nestjs/common';
import { ConfigModule } from 'nestjs-config';

const getEnvPath = (envName: string) => {
  let env = `.env.${envName}`;
  const p = (e: string) => path.resolve(__dirname, '..', '..', e);

  try {
    fs.readFileSync(p(env));
  } catch (error) {
    env = '.env';
  }

  return p(env);
};

@Module({
  imports: [
    ConfigModule.load(
      path.resolve(__dirname, '..', 'config/**/!(*.d).{ts,js}'),
      { path: getEnvPath(process.env.NODE_ENV) },
    ),

  ],
  exports: [ConfigModule],

})
export class InfraModule {}
